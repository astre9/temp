package com.test.room.database.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.test.room.database.models.Food;

import java.util.List;

@Dao
public interface FoodDao {
    @Query("SELECT * FROM Food")
    List<Food> getAll();

    @Query("SELECT * FROM Food WHERE uid IN (:foodIds)")
    List<Food> loadAllByIds(int[] foodIds);

    @Query("SELECT * FROM Food WHERE name LIKE :first LIMIT 1")
    Food findByName(String first, String last);

    @Insert
    public void insertFood(Food food);

    @Delete
    void delete(Food food);
}