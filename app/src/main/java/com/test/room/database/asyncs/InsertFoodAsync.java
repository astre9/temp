package com.test.room.database.asyncs;

import android.content.Context;
import android.os.AsyncTask;

import com.test.room.database.DataBase;
import com.test.room.database.models.Food;

public class InsertFoodAsync extends AsyncTask<Food, Void, Void> {

    private Context context;

    public InsertFoodAsync(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Food... lists) {
        DataBase dataBase = DataBase.getInstance(context);
        for (int i = 0; i < lists.length; i++) {
            dataBase.getDatabase().cityDAO().insertFood(lists[i]);
        }
        return null;
    }
}