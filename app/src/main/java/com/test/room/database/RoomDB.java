package com.test.room.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.test.room.database.daos.FoodDao;
import com.test.room.database.models.Food;

@Database(entities = {Food.class}, version = 1, exportSchema = false)
public abstract class RoomDB extends RoomDatabase {

    public abstract FoodDao cityDAO();

}