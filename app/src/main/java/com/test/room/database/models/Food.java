package com.test.room.database.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Food {
    @PrimaryKey
    public int fid;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "calories")
    public int calories;

    @ColumnInfo(name = "protein")
    public int protein;

    @ColumnInfo(name = "carb")
    public int carb;

    @ColumnInfo(name = "fat")
    public int fat;

}