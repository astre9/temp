package com.test.room;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.test.room.database.asyncs.InsertFoodAsync;
import com.test.room.database.models.Food;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        saveFood();
    }

    private void saveFood() {
        Food[] foods = new Food[10];
        for (int i = 0; i < 10; i++) {
            Food food = new Food();
            food.name = "Nume " + i;
            food.protein= i;
            foods[i] = food;
        }
        new InsertFoodAsync(this).execute(foods);
    }
}
